﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour 
{
	// reference to the player (sphere) which is linked to this script in the Inspector
	// odd that it appears as "Player" (capitalized) there but is lower-case here... 
	public GameObject player;

	// holds offset value (only editable here as a private variable)
	private Vector3 offset;

	// Use this for initialization
	void Start () 
	{
		// uses the camera's current transform position 
		offset = transform.position;
	}
	
	// Update is called once per frame
	// for gathering last-know states, use LateUpdate
	void LateUpdate () 
	{
		transform.position = player.transform.position + offset;
	}
}
