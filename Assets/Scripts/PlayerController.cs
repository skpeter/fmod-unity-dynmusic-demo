﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{	
	// public variable will appear in Unity inspector for all objects with this attached script
	public float speed;

	// display for current health
	public GUIText countText;
	public GUIText distText;

	// variable to hold health value and distance from capsule
	private int health;
	public float distance;

	//rigidbody to be stored
	Rigidbody rbody;

	//FMOD stuff
	FMOD.Studio.EventInstance mEvent;
	FMOD.Studio.ParameterInstance hpParam;
	FMOD.Studio.ParameterInstance dist;

	GameObject capsule;

	void Start()
	{
		// set count to 10 when the game begins
		health = 100;

		// see function below
		SetCountText ();
		rbody = GetComponent<Rigidbody> ();

		//call the event from the FMOD project's bank
		mEvent = FMOD_StudioSystem.instance.GetEvent ("event:/main/Music");
		//start up music
		mEvent.start();
		//link the ParameterInstance variable with the actual parameter in FMOD
		mEvent.getParameter("HP", out hpParam);
		mEvent.getParameter("Capsule Distance", out dist);

		capsule = GameObject.Find ("Capsule");
		hpParam.setValue (health);
	}

	// use this update for changes that require physics
	void FixedUpdate ()
	{
			// grabs player input from the keyboard and transfers to Rigidbody physics component
			float moveHorizontal = Input.GetAxis ("Horizontal");
			float moveVertical = Input.GetAxis ("Vertical");

			// new Vector3 variable uses keyboard input for its values: x = horiz key value, y= 0.0f (ball doesn't move up) and z = vert key value
			Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
			
			// applies input and modulates with speed value; Time.deltaTime smoothes the whole process
			rbody.AddForce(movement * speed * Time.deltaTime);

	}

	void Update()
	{
		//calculate distance between cylinder and player
		distance = Vector3.Distance(this.transform.position, capsule.transform.position);
		//in order to then throw it over to the distance parameter for fmod
		dist.setValue (distance);
		distText.text = "Distancia da Capsula: " + dist.ToString();
	}

	// called every time a trigger collider is touched
	void OnTriggerEnter(Collider other)
	{
		// if the tag matches "PickUpGood", make it inactive
		// tags must be set in the Unity editor Tag List; tag is applied to the Prefab so that all instances are tagged identically
		if (other.gameObject.tag == "PickUpGood") 
		{
			Debug.Log ("Colided with good pickup");
			//disables pickup's collider and renderer so it doesn't interact with player in any way temporarily
			other.gameObject.GetComponent<Collider>().enabled = false;
			other.gameObject.GetComponent<Renderer>().enabled = false;
			// sets up the timer on the pickup's behaviour script
			other.gameObject.GetComponent<PickUpBehaviour>().timerGo = true;
			// increments the health value
			health += 5;
			// update text display
			SetCountText ();

			//throws health value into the parameter
			hpParam.setValue (health);
		}

		// if tag matches "PickUpBad", make it inactive as well
		if (other.gameObject.tag == "PickUpBad") 
		{
			Debug.Log ("Colided with bad pickup");
			//disables pickup's collider and renderer so it doesn't interact with player in any way temporarily
			other.gameObject.GetComponent<Collider>().enabled = false;
			other.gameObject.GetComponent<Renderer>().enabled = false;
			// sets up the timer on the pickup's behaviour script
			other.gameObject.GetComponent<PickUpBehaviour>().timerGo = true;
			// decrements the health value
			health -= 5;
			//update text display
			SetCountText ();

			//throws health value into the parameter
			hpParam.setValue (health);

		}

	}

	// set starting value of GUI text display and print to the GUI Text field
	void SetCountText()
	{
		countText.text = "Saude: " + health.ToString();
	}

}