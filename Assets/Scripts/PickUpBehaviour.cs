﻿using UnityEngine;
using System.Collections;

public class PickUpBehaviour : MonoBehaviour {

	float umTimer = 3f;
	public bool timerGo = false;

	// Use this for initialization
	void Start () {
	
	}
	
	void Update () 
	{
		if (timerGo == true) 
		{
			if (umTimer > 0) 
			{
				umTimer -= Time.deltaTime;
			}
		}
		if (umTimer <= 0)
		{
				Debug.Log ("Pickup Respawned");
				gameObject.GetComponent<Collider>().enabled = true;
				gameObject.GetComponent<Renderer>().enabled = true;
				umTimer = 3f;
				timerGo = false;
		}
	}
}
