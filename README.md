# README #

### What is this repository for? ###

* This is a tech demo whose objective is to show FMOD's integration with Unity in order to add dynamic music to your game.
For the moment it only works with the "stems" approach to dynamic music, which resumes into separating your music in instrument layers, then adding/subtracting them to the game as needed. Support for other methods of dynamic music are not yet planned, but could work out in the future.
* Version: None yet as of this readme
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Configuration
Clone this repo to your Unity's projects folder (at public documents folder by default). From there all you need to do is open your project with Unity, everything should be set up for that. Of course, this means you need Unity 5.0.1 for this to work (does not need to be Pro).

### Contribution guidelines ###

I still have no idea how to manage this whole open-source coding deal, so basically push me anything and I'll give it a try.

### Who do I talk to? ###

Just refer to me about anything: @skpeter